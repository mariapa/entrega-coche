from coche import Coche
import unittest

class TestCoche(unittest.TestCase):

    def test_caracteristicas1(self): #Debe ir el test primero
        coche1=Coche('azul', 'Citroen','C5', '1899 FKF', 50)
        caracteristicas1=coche1.ImprimirCaracterísticas()
        self.assertEqual(caracteristicas1, None)

    def test_caracteristicas2(self): #Debe ir el test primero
        coche2= Coche('rojo', 'Chevrolet', 'Corvette','7766 FKF', 20)
        caracteristicas2=coche2.ImprimirCaracterísticas()
        self.assertEqual(caracteristicas2, None)


    def test_aceleracion1(self):
        coche1=Coche('azul', 'Citroen','C5', '1899 FKF', 50)
        acelerar1=coche1.Acelerar()
        self.assertEqual(acelerar1,70)
    
    def test_aceleracion2(self):
        coche2= Coche('rojo', 'Chevrolet', 'Corvette','7766 FKF', 20)
        acelerar2=coche2.Acelerar()
        self.assertEqual(acelerar2,40)

    
    def test_frenar1(self):
        coche1=Coche('azul', 'Citroen','C5', '1899 FKF', 50)
        frenar1=coche1.Frenar()
        self.assertEqual(frenar1,40)

    def test_frenar2(self):
        coche2= Coche('rojo', 'Chevrolet', 'Corvette','7766 FKF', 20)
        frenar2=coche2.Frenar()
        self.assertEqual(frenar2,10)


if __name__ == "__main__":
    unittest.main()