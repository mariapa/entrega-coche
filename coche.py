class Coche: 
    def __init__(self, color, marca, modelo, matricula, velocidad): 
        self.__color=color
        self.__marca = marca
        self.__modelo = modelo
        self.__matricula = matricula
        self.__velocidad= velocidad
    def ImprimirCaracterísticas(self):
        print ("El coche es un {marca} {modelo} de color {color} con matricula {matricula}".format(marca=self.__marca,modelo=self.__modelo, color=self.__color, matricula=self.__matricula))
    def Acelerar(self): 
        return self.__velocidad+20
    def Frenar(self):
        return self.__velocidad-10
    

#Defino los coches

coche1 = Coche('azul', 'Citroen','C5', '1899 FKF', 50)
coche2= Coche('rojo', 'Chevrolet', 'Corvette','7766 FKF', 20)

coches = [coche1, coche2]

for car in coches:
    car.ImprimirCaracterísticas()
    print('El coche acelera tomando una velocidad final de:'+str(car.Acelerar())+' m/s')
    print('El coche frena tomando una velocidad final de:'+str(car.Frenar())+' m/s')

